 function [E,V]=MeanVar(N,beta,I_ini)
E=0;
V=0;
    for i=I_ini:N-1
        E=E+N/(i*(N-i));
        V=V+N^2/(beta*i*(N-i))^2;
    end
end
