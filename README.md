# Información de la plantilla para informes largos

## Autores

* Lubián Arenillas, Daniel: [@danlub](https://gitlab.com/danlub)

* Moreno Jódar, Carlos: [@CarlosMJ](https://gitlab.com/CarlosMJ)

## Repositorio del proyecto:

[https://gitlab.com/idr-muse/report-template](https://gitlab.com/idr-muse/report-template)

## Versiones congeladas:

Última versión congelada aquí: OJO. 

La última es la `v3.0-peter` (como manda la lógica)

Se encuentran en la siguiente dirección: [https://gitlab.com/idr-muse/report-template/tags](https://gitlab.com/idr-muse/report-template/tags)

## Versión en desarrollo:

Se encuentra en: [https://gitlab.com/idr-muse/report-template/tree/master](https://gitlab.com/idr-muse/report-template/tree/master)

## Agradecimientos

* Criado Pernía, David; por añadir la parte de acrónimos
